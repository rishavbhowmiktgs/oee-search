////////////////////////////////////////////
const mainfest = require('./manifest.json');
const {rank_common_id} = require('./search/mandate')
const server = require('http').createServer();
const io = require('socket.io')(server);

//Main Function...................................................................................
(async _ => {
  const {Search, Query} = await require('./search/index')()

  io.on('connection', client => {
    var search_session = null
  
    client.on('start_session', data => {
      try {
        //console.log(data);
        if (
              typeof data.user_id != 'string'         || data.user_id.length != 24
          ||  typeof data.organisation_id != 'string' || data.organisation_id.length != 24
        ) throw (Error("Illegal input"), {code:422})
        //creating new session
        search_session = new Search(data.user_id, data.organisation_id)
        search_session.load_map()
        .then( _ => {
          client.emit('session_ready', {})
        })
      } catch (e) {
        //return error & close the connection with the socket
        console.log(e);
        search_session = null
        client.error(e)
        client.disconnect(true)
      }
    })
  
    client.on('query', async data => {
      try {
        const q = new Query(data.query)
        if( !(q.tags.length + q.words.length + q.numbers.length) ) return
        var results = { __org:[], __tg:[], __panel:[]}
        var result_len = 0;
        const all_tags = q.tags.length<1
        if(all_tags || q.tags.includes('__org')){
          results.__org = rank_common_id(
            await search_session.search_organisations(q.words.concat(q.numbers)),
            "__org"
          )
        }
        result_len += results.__org.length
        if(all_tags || result_len<4 || q.tags.includes('__tg')){
          results.__tg = rank_common_id(
            //search_machines have seperate case with number due to unique TG id...s
            await search_session.search_machines(q.words, q.numbers),
            "__tg"
          )
        }
        result_len += results.__tg.length
        if(all_tags || result_len<4 || q.tags.includes('__panel')){
          results.__panel = rank_common_id(
            await search_session.search_panels(q.words.concat(q.numbers)),
            "__panel"
          )
        }
        /*
          need to rank and process the results
        */
        
       
        client.emit('results', results)

      } catch (e) {
        if( !(q.tags.length + q.words.length + q.numbers.length) ) return
        console.log("Err72", e);
      }
    })
  
    client.on('worker_query', async data => {
      try{
        const q = new Query(data.query)
        if( !(q.tags.length + q.words.length + q.numbers.length) ) return
        const results = rank_common_id(
          await search_session.search_workers(q.words.concat(q.numbers)),
          "__worker"
        )

        client.emit('results_worker_query', results)
      }catch (e) {
        console.log("Err90", e);
      }
    })
  
    client.on('load_machine', data => {
      if(search_session === null || !data.machine_id) return
      search_session.load_machine_map(data.machine_id)
      .then(()=>{
        client.emit('load_machine_result', {machine_id:data.machine_id, req_key:data.req_key})
      })
      .catch((e)=>{
        console.log(e);
      })
    })

    client.on('pp_query', async data => {
      try{
        const q = new Query(data.query)
        if( !(q.tags.length + q.words.length + q.numbers.length) ) return
        const results = rank_common_id(
          await search_session.search_machine_pps(q.words.concat(q.numbers)),
          "__pp"
        )

        client.emit('results_pp_query', results)
      }catch (e) {
        console.log("Err90", e);
      }
    })

    client.on('issues_query', async data => {
      try{
        const q = new Query(data.query)
        if( !(q.tags.length + q.words.length + q.numbers.length) ) return
        const results = rank_common_id(
          await search_session.search_machine_issues(q.words.concat(q.numbers)),
          "__issue"
        )

        client.emit('results_issues_query', results)
      }catch (e) {
        console.log("Err90", e);
      }
    })

    client.on('planedDowntime_query', async data => {
      try{
        const q = new Query(data.query)
        if( !(q.tags.length + q.words.length + q.numbers.length) ) return
        const results = rank_common_id(
          await search_session.search_machine_planedDowntime(q.words.concat(q.numbers)),
          "__downtime"
        )

        client.emit('results_planedDowntime_query', results)
      }catch (e) {
        console.log("Err90", e);
      }
    })


    client.on('disconnect', _ => { 
      //on disconnect
      console.log("Client disconnected");
      search_session = null
      client.disconnect(true)
    })
  
    //initate session with client
    client.emit('start_session', {})
  });
  
  server.listen(process.env.port || mainfest.port, ()=>{
    console.log("Listening");
  });
})()
//End of Main Function............................................................................

//unit testing
/*
(async ()=>{
  try{
    const {Search, Query} = await require('./search/index')()
    const q = new Query('Hellow world machine _ 124')
    console.log(q.tags, q.numbers, q.words)

    const search =  new Search("5d5aa3ba4dcf8a32f09bd9f3", "5d5bc271f6897812b84ba3dc")
    await search.load_map()
    console.log("SErver", Date.now())
    console.log("30", Date.now());
    console.log(await search.search_organisations(["bharat", "lab", "bhar"]))
    console.log("32", Date.now());
    console.log(await search.search_machines(["005"]))
    console.log("34", Date.now());
    console.log(await search.search_panels(["new", "panel"]))
  }catch(e){
    console.log(e);
  }
})()
*/