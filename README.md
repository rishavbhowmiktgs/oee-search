# Oee Search
Search Machines, Panels, Production Plans etc

## Components of the system

### Sitemap
Contains the format for the links that would be produced for the user to surf with the smart factory.

Sitemap tupple are used to from appropriate link that will open on user's browser, when user opens the search results.

### Resemblemap
This map has contains data that would be used for identification & Normalisation of tags present in the search query.

### Normalizer
Normalizer performs series of operations to Normalize, the query input to form well suited for searching and identify approprite tags crucial for directing the search operation.

#### Normalization
#### Query filteration
Identify words, numbers & tags(normalized) present in the query

#### Query Interpretor

### DB_Scanner
Performs read operation on the main MongoDB Database
- Perform search in organisation collection
- Perfrom Search on an Organisation
- Perform Search on a Machine doc

### Result Ranker

### Result Parser

# Adding a new search

- `db_scanner class`
- `db_scanner.js exports`
- `class Search`
-- Update `Search.load_map()`
-- Create `Search.search_funtion()`
- Create `ParseResults.__filter_funtion()` in `mandate.js`
- Add interface in sockets