const manifest = require('../manifest')
const site_map = require('./sitemap')
const Normalizer = require('./normalizer')
const MongodbObjectId = require('mongodb').ObjectId
const sqlite = require('sqlite3').verbose()


class Query{
    constructor(_query){
        const query   = _query.toLowerCase().replace(/(?![a-z0-9])./g, " ")
        //console.log(query);
        const q_terms = query.match(/(\d+)|([a-z]+)/g)
        //console.log(q_terms)
        const {q_numbers, q_words, q_tags} = Normalizer.identify(q_terms)
        this.numbers  = q_numbers
        this.words    = q_words
        this.tags     = q_tags
    }
}

class Search{
    constructor(user_id, organisation_id){
        this.user_id         = MongodbObjectId(user_id)
        this.organisation_id = MongodbObjectId(organisation_id)
        this.maps            = null
    }
    async load_map(){
        const this_class = this
        try {
            //console.log("Mem30", process.memoryUsage());
            this_class.maps = new sqlite.Database(":memory:")
            const organisations = await Search.DB_Scanner.scan_organisations(this_class.user_id)
            const { organisation, machines, panels, workers} = await Search.DB_Scanner.scan_one_organisation(this_class.organisation_id)
            //console.log(organisation, machines, panels);
            /*
                # Creating Tables:-
                - organisations
                - machines
                - panels
                - workers
                # Creating Index 'name_index' in table organisations, machines, panels, workers
            */
            await new Promise((resolve, reject)=>{
                this_class.maps.serialize( () => {
                    //organisations table
                    this_class.maps.run(`
                        CREATE TABLE IF NOT EXISTS organisations    ( name text, _id VARCHAR(24) );
                    `, ()=>{
                        //organisations table indexing
                        this_class.maps.run("CREATE INDEX organisations_index ON organisations  ( name );")
                    })
                    //machines table
                    this_class.maps.run(`
                        CREATE TABLE IF NOT EXISTS machines         ( name text, _id VARCHAR(24) );
                    `, ()=>{
                        //machines table indexing
                        this_class.maps.run("CREATE INDEX machines_index ON machines            ( name );")
                    })
                    //panels table
                    this_class.maps.run(`
                        CREATE TABLE IF NOT EXISTS panels           ( name text, _id VARCHAR(24) );
                    `, ()=>{
                        //panels table indexing
                        this_class.maps.run("CREATE INDEX panels_index ON panels                ( name );")
                    })
                    //workers table
                    this_class.maps.run(`
                        CREATE TABLE IF NOT EXISTS workers           (name TEXT, _id VARCHAR(24) );
                    `, ()=>{
                        this_class.maps.run("CREATE INDEX workers_index ON workers              ( name );",
                            resolve
                        )
                    })

                })
            })
            //console.log(69);
            /*
                # Insert [organisations] content into table organisations
                # Inserting :-
                - name = organisations[i].organisationName
                - _id  = organisations[i]._id.toString()
            */
            await new Promise((resolve, reject)=>{
                this_class.maps.serialize( () => {
                    if(organisations){
                        var stmt = this_class.maps.prepare("INSERT INTO organisations   VALUES ( ?, ?)")
                        for (const i in organisations) {
                            stmt.run([organisations[i].organisationName, organisations[i]._id.toString()])
                        }
                        stmt.finalize(resolve)
                    }else resolve()
                })
            })
            //console.log(87);
            /*
                # Inserting [machines] content into table machines
                # Inserting :-
                - name = machines[i].machineName + \s + machines[i].tgId
                - _id  = machines[i]._id.toString()
            */
            await new Promise((resolve, reject)=>{
                this_class.maps.serialize( () => {
                    if(machines){
                        var stmt = this_class.maps.prepare("INSERT INTO machines        VALUES ( ?, ?)")
                        for (const i in machines) {
                            stmt.run([`${machines[i].machineName} ${machines[i].tgId}`, machines[i]._id.toString()])
                        }
                        stmt.finalize(resolve)
                    }else resolve()
                })
            })
            //console.log(105);
            /*
                # Inserting [panels] content into table panels
                # Check if panel has content
                # Inserting :-
                - name = panels[i].pname
                - _id  = panels[i]._id.toString()
            */
            await new Promise((resolve, reject)=>{
                this_class.maps.serialize( () => {
                    if(panels){
                        var stmt = this_class.maps.prepare("INSERT INTO panels        VALUES ( ?, ?)")
                        for (const i in panels) {
                            stmt.run([panels[i].pname, panels[i]._id.toString()])
                        }
                        stmt.finalize(resolve)
                    }else resolve()
                })
            })
            //console.log(124);
            /*
                # Inseting [workers] content into table workers
                # check if workers has content
                # Inserting :-
                - name
                - _id
            */
           await new Promise((resolve, reject)=>{
                this_class.maps.serialize( () => {
                    if(workers){
                        var stmt = this_class.maps.prepare("INSERT INTO workers   VALUES ( ?, ?)")
                        for (const i in workers) {
                            stmt.run([`${workers[i].wname} | ${workers[i].wemail}`, workers[i]._id.toString()])
                        }
                        stmt.finalize(resolve)
                    }else resolve()
                } )
           })

            //console.log("Mem56", process.memoryUsage());
            return
        } catch (e) {
            throw e;
        }
    }
    async load_machine_map(machine_id){
        const this_class = this
        const {issues, planedDowntime, pps} = await Search["DB_Scanner"].scan_one_machine(machine_id)
        //# Create Tables
        // - issues table
        await new Promise((resolve, reject)=>{
            this_class.maps.run(`DROP TABLE IF EXISTS issues;`, ()=>{
                this_class.maps.run(`
                    CREATE TABLE issues ( _id VARCHAR(24), name TEXT, display TEXT );
                `, resolve )
            })
        })
        // - pps table
        await new Promise((resolve, reject)=>{
            this_class.maps.run(`DROP TABLE IF EXISTS pps;`, ()=>{
                this_class.maps.run(`
                    CREATE TABLE pps ( _id VARCHAR(24), name TEXT, display TEXT );
                `, resolve )
            })
        })
        // - downtimes table
        await new Promise((resolve, reject)=>{
            this_class.maps.run(`DROP TABLE IF EXISTS downtimes;`, ()=>{
                this_class.maps.run(`
                    CREATE TABLE downtimes ( _id VARCHAR(24), name TEXT, display TEXT );
                `, resolve )
            })
        })

        /*
            # clear issues table
            # Inseting [issues] content into table issues
            # check if issues has content
            # Inserting :-
            - name
            - _id
            - display
        */
       console.log(issues);
        await new Promise((resolve, reject)=>{
            this_class.maps.serialize( () => {
                if(issues){
                    var stmt = this_class.maps.prepare("INSERT INTO issues   VALUES ( ?, ?, ?)")
                    for (const i in issues) {
                        stmt.run([issues[i]._id, issues[i].name, issues[i].display])
                    }
                    stmt.finalize(resolve)
                }else resolve()
            } )
        })
        /*
            # clear pps table
            # Inseting [pp] content into table pps
            # check if pps has content
            # Inserting :-
            - name
            - _id
            - display
        */
        await new Promise((resolve, reject)=>{
            if(pps){
                var stmt = this_class.maps.prepare("INSERT INTO pps   VALUES ( ?, ?, ?)")
                for (const i in pps) {
                    stmt.run([pps[i]._id, pps[i].name, pps[i].display])
                }
                stmt.finalize(resolve)
            }else resolve()
        })
        /*
            # clear downtimes table
            # Inseting [planedDowntime] content into table downtimes
            # check if pps has content
            # Inserting :-
            - name
            - _id
            - display
        */
        await new Promise((resolve, reject)=>{
            if(planedDowntime){
                var stmt = this_class.maps.prepare("INSERT INTO downtimes   VALUES ( ?, ?, ?)")
                for (const i in planedDowntime) {
                    stmt.run([planedDowntime[i]._id, planedDowntime[i].name, planedDowntime[i].display])
                }
                stmt.finalize(resolve)
            }else resolve()
        })
        return
    }
    async search_organisations(words){
        //search <- words
        const this_class = this
        try {
            if(!this_class.maps) throw Object.assign(Error('Map not loaded'), {code:135})
            var results = []
            //finding match for every word in words
            for(var w in words){
                const good_word = `'%${JSON.stringify(words[w]).replace(/^\"/, "").replace(/\"$/, "")}%'`
                const rows = await new Promise( resolve =>
                        this_class.maps.all(`
                            SELECT name, _id from organisations WHERE LOWER(name) LIKE ${good_word};
                        `, (err, rows)=>{
                            if(err) reject(err)
                            resolve(rows)
                        })
                    )
                results = results.concat(rows)
            }
            return results
        } catch (e) {
            throw (e)
        }
    }
    async search_machines(words, numbers){
        //search <- words || tg+number 
        const this_class = this
        try {
            if(!this_class.maps) throw Object.assign(Error('Map not loaded'), {code:135})
            var results = []
            //finding match for every word in words
            for(var w in words){
                const good_word = `'%${JSON.stringify(words[w]).replace(/^\"/, "").replace(/\"$/, "")}%'`
                const rows = await new Promise((resolve, reject)=>{
                        this_class.maps.all(`
                            SELECT name, _id from machines WHERE LOWER(name) LIKE ${good_word}
                        `, (err, rows)=>{
                            if(err) reject(err)
                            resolve(rows)
                        })
                    })
                results = results.concat(rows)
            }
            //finding match for every number in numbers
            for(var w in numbers){
                const good_word = `'%${JSON.stringify(numbers[w]).replace(/^\"/, "").replace(/\"$/, "")}%'`
                const rows = await new Promise((resolve, reject)=>{
                        this_class.maps.all(`
                            SELECT name, _id from machines WHERE LOWER(name) LIKE ${good_word}
                        `, (err, rows)=>{
                            if(err) reject(err)
                            resolve(rows)
                        })
                    })
                results = results.concat(rows)
            }
            return results
        } catch (e) {
            throw e
        }
    }
    async search_panels(words){
        //search <- words
        const this_class = this
        try {
            if(!this_class.maps) throw Object.assign(Error('Map not loaded'), {code:135})
            var results = []
            //finding match for every word in words
            for(var w in words){
                const good_word = `'%${JSON.stringify(words[w]).replace(/^\"/, "").replace(/\"$/, "")}%'`
                const rows = await new Promise( resolve =>
                        this_class.maps.all(`
                            SELECT name, _id from panels WHERE LOWER(name) LIKE ${good_word};
                        `, (err, rows)=>{
                            if(err) reject(err)
                            resolve(rows)
                        })
                    )
                results = results.concat(rows)
            }
            return results
        } catch (e) {
            throw (e)
        }
    }
    async search_workers(words){
        const this_class = this
        try {
            if(!this_class.maps) throw Object.assign(Error('Map not loaded'), {code:135})
            var results = []
            //finding match for every word in words
            for(var w in words){
                const good_word = `'%${JSON.stringify(words[w]).replace(/^\"/, "").replace(/\"$/, "")}%'`
                const rows = await new Promise( resolve =>
                        this_class.maps.all(`
                            SELECT name, _id from workers WHERE LOWER(name) LIKE ${good_word};
                        `, (err, rows)=>{
                            if(err) reject(err)
                            resolve(rows)
                        })
                    )
                results = results.concat(rows)
            }
            return results
        } catch (e) {
            throw (e)
        }
    }
    async search_machine_issues(words){
        const this_class = this
        try {
            if(!this_class.maps) throw Object.assign(Error('Map not loaded'), {code:135})
            var results = []
            //finding match for every word in words
            for(var w in words){
                const good_word = `'%${JSON.stringify(words[w]).replace(/^\"/, "").replace(/\"$/, "")}%'`
                const rows = await new Promise( resolve =>
                        this_class.maps.all(`
                            SELECT name, _id, display from issues WHERE LOWER(name) LIKE ${good_word};
                        `, (err, rows)=>{
                            if(err) reject(err)
                            resolve(rows)
                        })
                    )
                results = results.concat(rows)
            }
            return results
        } catch (e) {
            throw (e)
        }
    }
    async search_machine_pps(words){
        const this_class = this
        try {
            if(!this_class.maps) throw Object.assign(Error('Map not loaded'), {code:135})
            var results = []
            //finding match for every word in words
            for(var w in words){
                const good_word = `'%${JSON.stringify(words[w]).replace(/^\"/, "").replace(/\"$/, "")}%'`
                const rows = await new Promise( resolve =>
                        this_class.maps.all(`
                            SELECT name, _id, display from pps WHERE LOWER(name) LIKE ${good_word};
                        `, (err, rows)=>{
                            if(err) reject(err)
                            resolve(rows)
                        })
                    )
                results = results.concat(rows)
            }
            return results
        } catch (e) {
            throw (e)
        }
    }
    async search_machine_planedDowntime(words){
        const this_class = this
        try {
            if(!this_class.maps) throw Object.assign(Error('Map not loaded'), {code:135})
            var results = []
            //finding match for every word in words
            for(var w in words){
                const good_word = `'%${JSON.stringify(words[w]).replace(/^\"/, "").replace(/\"$/, "")}%'`
                const rows = await new Promise( resolve =>
                        this_class.maps.all(`
                            SELECT name, _id, display from downtimes WHERE LOWER(name) LIKE ${good_word};
                        `, (err, rows)=>{
                            if(err) reject(err)
                            resolve(rows)
                        })
                    )
                results = results.concat(rows)
            }
            return results
        } catch (e) {
            throw (e)
        }
    }
}

module.exports = async (callback)=>{
    try {
        Search["DB_Scanner"] = await require('./db_scanner')()
        if(typeof callback == 'function')
            callback(null, Search, Query)
        return {Search : Search, Query : Query}
    } catch (e) {
        if(typeof callback == 'function')
            callback(e, null)
        else
            throw e
    }
}