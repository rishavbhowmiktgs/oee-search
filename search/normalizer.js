class Normalizer{
    static normalize_word(word){
        //sqlite will be integrated later
        return Normalizer.resemblemap[word] || null
    }
    static identify(_words){
        if(!_words || !_words.length) return {q_numbers: [], q_words: [], q_tags: []}
        const numbers   = new Set() //set of numbers
        const words     = new Set() //set of strictly normalized words
        const all_words = new Set() //set of all normalized & unnormalized words
        _words.forEach(word => {
            if((typeof word != 'string') || (!word.length)) return
            const n = Number(word);
            if(Number.isNaN(n)){
                const normal = Normalizer.normalize_word(word)
                if(normal)
                    words.add(normal)
                all_words.add(word)
            }
            else
                numbers.add(n)
        })
        return {q_numbers: [...numbers], q_words: [...all_words], q_tags: [...words]}
    }
}
Normalizer.resemblemap = require('./resemblemap')

module.exports = Normalizer