//This module is higly dependent on sitemap
//And any changes in Sitemap should be verified with the dependecies of this module
const site_map = require('./sitemap')

class ParseResults{
    static __org(display, org_id, post='0'){
        return {
            url: site_map['console_organisation']['path']
                .replace('<organisation_id>', org_id)
                .replace('<user_post>'      , post)         //default post set to 0
        ,   display: display
        }
    }
    static __tg(display, machine_id){
        return {
            url: site_map['machine']['path']
                .replace('<machine_id>', machine_id)
        ,   display: display
        }
    }
    static __panel(display, panel_id){
        return {
            url: site_map['panel']['path']
                .replace('<panel_id>', panel_id)
        ,   display: display
        }
    }
    static __worker(display, worker_id){
        return {
            _id: worker_id,
            display : display
        }
    }
    static __pp(display, _id){
        return {
            _id,
            display
        }
    }
    static __issue(display, _id){
        return {
            _id,
            display
        }
    }
    static __downtime(display, _id){
        return {
            _id,
            display
        }
    }
}

function rank_common_id(list, tag){
    var all = {}
    for(const i in list){
        if(all[list[i]._id])
            all[list[i]._id].score ++
        else
            all[list[i]._id] = {
                result: ParseResults[tag](list[i].display||list[i].name, list[i]._id), score:1 
            }
    }
    //creating a final array
    var results = []
    for (const i in all) {
        results.push(all[i])
    }
    return results
}

module.exports = {
    rank_common_id : rank_common_id
}