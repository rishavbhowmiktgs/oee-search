const manifest          = require('../manifest.json')
const private_manifest  = require("../../private/manifest.json")
const mongodb           = require('mongodb')

class DB_Scanner{
    static async scan_organisations(_id){
        try {
            const result = await DB_Scanner.USERS_COL.findOne(
                    {_id:mongodb.ObjectId(_id)},
                    {"projection":{"organisations":1, "_id":0}}
                )
            if(!result) return null
            var org_id_list = []
            result.organisations.forEach(orgs => {
                org_id_list.push({_id:orgs.organisationId})
            })
            const organisations = await DB_Scanner.ORGANISATION_COL.find(
                    {"$or":org_id_list}
                ).project(
                    {"organisationName":1}
                ).toArray()
            return organisations
        } catch (e) {
            throw e
        }
    }
    static async scan_one_organisation(org_id){
        try {
            const organisation = await DB_Scanner.ORGANISATION_COL.findOne(
                    {"_id":org_id},
                    {"projection":{"machines":1, "panels":1, "_id":0, "workers":1}}
                )
            if(!organisation) return {organisation:null, machines:null, panels:null}
            const panels            = organisation.panels
            const machine_id_list   = organisation.machines
            const workers           = organisation.workers
            const machine_mongo_ops = Array()

            machine_id_list.forEach(machine_id => {
                machine_mongo_ops.push({"_id":machine_id})
            })

            const machines = await DB_Scanner.MACHINE_COL.find(
                {"$or":machine_mongo_ops}
            ).project({"machineName":1, "tgId":1}).toArray()

            return {organisation:organisation, machines:machines, panels:panels, workers:workers}
        } catch (e) {
            throw e
        }
    }
    //scan machines of organisation
    static async scan_one_machine(machine_id){
        const machine = await DB_Scanner.MACHINE_COL.findOne(
            {_id : mongodb.ObjectId(machine_id)}
        )
        if(machine == null) return null;
        const pp_id_list = machine.productionPlan.map((i)=>{
            return { _id : mongodb.ObjectId(i) }
        })
        const issues = machine.issues?machine.issues.map((i)=>{
            return {_id : i, name : i, display : i}
        }):null
        const planedDowntime = machine.planedDowntime?machine.planedDowntime.map((i)=>{
            if(i)
                return {_id : i._id, name: i.cause, display:`${i.cause} [${i.from} - ${i.to}]`}
        }):null
        const pps = (await DB_Scanner.PPS_COL.find({
            "$or":pp_id_list
        }).toArray()).map((i)=>{
            return { _id : i._id, name:i.productionName, display:`${i.productionName} [${i.from} - ${i.to}]`}
        })

        return {issues, planedDowntime, pps}
    }
}
DB_Scanner.client = new mongodb.MongoClient(
    manifest.mongodb_uri
    .replace("<username>",  private_manifest.mongodb.username)
    .replace("<password>",  private_manifest.mongodb.password)
    .replace("<host>"    ,  private_manifest.mongodb.host),
    manifest.mongodbOptions
)
module.exports = async ()=>{
    try {
        await DB_Scanner.client.connect()
        DB_Scanner.MACHINE_COL      = DB_Scanner.client.db('oee').collection('machines')
        DB_Scanner.ORGANISATION_COL = DB_Scanner.client.db('oee').collection('organisations') //this holds the panels
        DB_Scanner.PPS_COL          = DB_Scanner.client.db('oee').collection('pps')
        DB_Scanner.USERS_COL        = DB_Scanner.client.db('oee').collection('users')
        return DB_Scanner
    } catch (e) {
        throw e
    }
}

//unit test
/*
DB_Scanner.client.connect().then( _ => {
    DB_Scanner.MACHINE_COL      = DB_Scanner.client.db('oee').collection('machines')
    DB_Scanner.PPS_COL          = DB_Scanner.client.db('oee').collection('pps')
    DB_Scanner.scan_one_machine('5e467277d75a201dd3b61035')
    .then((res)=>{console.log(res);})
})*/