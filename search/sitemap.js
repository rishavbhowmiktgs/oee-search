const oee_ui_host = require('../manifest').oee_ui_host

const sitemap = {
    "console" : {
        path : `${oee_ui_host}console`,
        components : []
    },
    "console_organisation" : {
        path : `${oee_ui_host}console?organasationseclected=<organisation_id>/<user_post>`,
        components : ['<organisation_id>', '<user_post>']
    },
    "machine" : {
        path : `${oee_ui_host}machine?machineId=<machine_id>`,
        components : ['<machine_id>']
    },
    "panel"   : {
        path : `${oee_ui_host}panel/<panel_id>`,
        components : ['<panel_id>']
    }
}

module.exports = sitemap