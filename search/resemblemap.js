const resemblemap = {
    "org"           :"__org",
    //look for tg ids
    "tg"            :"__tg",
    "tgid"          :"__tg",
    "tgsid"         :"__tg",
    "machine"       :"__tg",
    "id"            :"__tg",
    "machine number":"__tg",
    //look for panels
    "panel"         :"__panel",
    "p"             :"__panel",
    "pan"           :"__panel",
    "panl"          :"__panel",
    "pnl"           :"__panel",
    "collection"    :"__panel"
}

module.exports = resemblemap